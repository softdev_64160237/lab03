/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package softdev.lab03;

/**
 *
 * @author EliteCorps
 */
public class Lab03 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public static boolean checkWin(char[][] board, char turn) {
        return checkRow(board, turn) | checkCol(board, turn) | checkDiagnol(board, turn);
    }
    
    private static boolean checkRow(char[][] board,char turn) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '-' && board[i][0] == turn && board[i][1] == turn && board[i][2] == turn) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(char[][] board,char turn) {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '-' && board[0][i] == turn && board[1][i] == turn && board[2][i] == turn) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkDiagnol(char[][] board,char turn) {
        for (int i = 0; i < 3; i++) {
            if (board[0][0] != '-' && board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
                return true;
            }
        }
        
        for (int i = 0; i < 3; i++) {
            if (board[0][2] != '-' && board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkDraw(char[][] board, char turn) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return !checkWin(board, turn);
    }
}
