/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package softdev.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author EliteCorps
 */
public class testOX {
    
    public testOX() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_Row1_PlayerXWin() {
        char[][] board = {
            {'X','X','X'},
            {'-','-','-'},
            {'-','-','-'}
        };
        char turn = 'X';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Row2_PlayerOWin() {
        char[][] board = {
            {'-','-','-'},
            {'O','O','O'},
            {'-','-','-'}
        };
        char turn = 'O';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Row3_PlayerXWin() {
        char[][] board = {
            {'-','-','-'},
            {'-','-','-'},
            {'X','X','X'}
        };
        char turn = 'X';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Column1_PlayerOWin() {
        char[][] board = {
            {'O','-','-'},
            {'O','-','-'},
            {'O','-','-'}
        };
        char turn = 'O';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Column2_PlayerXWin() {
        char[][] board = {
            {'-','X','-'},
            {'-','X','-'},
            {'-','X','-'}
        };
        char turn = 'X';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Column3_PlayerOWin() {
        char[][] board = {
            {'-','-','O'},
            {'-','-','O'},
            {'-','-','O'}
        };
        char turn = 'O';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Diagnol1_PlayerXWin() {
        char[][] board = {
            {'X','-','-'},
            {'-','X','-'},
            {'-','-','X'}
        };
        char turn = 'X';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_Diagnol2_PlayerOWin() {
        char[][] board = {
            {'-','-','O'},
            {'-','O','-'},
            {'O','-','-'}
        };
        char turn = 'O';
        assertEquals(true, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_NoWin() {
        char[][] board = {
            {'O','X','O'},
            {'X','X','O'},
            {'O','O','X'}
        };
        char turn = 'O';
        assertEquals(false, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckWin_NoPlay() {
        char[][] board = {
            {'-','-','-'},
            {'-','-','-'},
            {'-','-','-'}
        };
        char turn = 'X';
        assertEquals(false, Lab03.checkWin(board, turn));
    }
    
    @Test
    public void testCheckDraw() {
        char[][] board = {
            {'O','X','O'},
            {'X','X','O'},
            {'O','O','X'}
        };
        char turn = 'O';
        assertEquals(true, Lab03.checkDraw(board, turn));
    }
    
    @Test
    public void testCheckDraw_NoDraw() {
        char[][] board = {
            {'O','X','O'},
            {'X','X','O'},
            {'x','O','O'}
        };
        char turn = 'O';
        assertEquals(false, Lab03.checkDraw(board, turn));
    }

}
